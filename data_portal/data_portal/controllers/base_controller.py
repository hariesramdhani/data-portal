from pyramid.httpexceptions import HTTPFound

class BaseController:
    def __init__(self):
        self.request = request

    @staticmethod
    def redirect(url):
         raise HTTPFound(url)
