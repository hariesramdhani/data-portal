from models.datasets import DatasetModel as dataset_model

import sys
sys.path.insert(0, '../data_portal')

from cornice import Service

_DATASETS = {}

#NOTE: For more Info http://cornice.readthedocs.io/en/latest/quickstart.html

datasets = Service(name='data_portal_api',
                 path='/api/datasets/{id}',
                 description="Data Portal API")

@datasets.get()
def get_dataset(request):
    """Returns the value.
    """
    key = request.matchdict['id']

    return dataset_model.get_all_metadata_for_dataset_by_id(key)

@datasets.post()
def set_dataset(request):
 
    key = request.matchdict['id']
    try:
        # the curl command will be used to post the information
        # curl –X POST http://dev-dp-pyramid.stemformatics.org:8080/datasets/1000 -d 
        #'{"dp_base_url":"https://swift.rc.nectar","dp_ensemble_version":"v86",
        #"dp_probes_file_location":"/1000.probes",
        #"dp_yugene_file_location":"/.bgonly_cumulative.txt"}'
        _DATASETS[key] = request.json_body

        # There are two things to implement : without TOKEN Now and with Token authentication later
        # Will call the model function to add the respective json values into the DB(dataset_metadata
        # table)

        # Check the flag for migration in POST Request
        if  "is_migrate" in _DATASETS[key]:
            if("True" in _DATASETS[key].values()):
                # If is_migrate flag is on that means this post is for migration purpose just to update DB
                
                if(dataset_model.set_metadata_for_migration(key,_DATASETS[key])==True):
                
                    message="Data has been migrated to respective DB"
                else:
                    message="An Exception occur in DB connectivity!"

            else:
                # if is_migrate flag is off that means this post is for insert into DB
                message ="Data has been added to respective DB"
    
        return {'status': message}

    except ValueError:
        return False
    return True