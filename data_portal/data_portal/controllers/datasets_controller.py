import json
from pyramid_handlers import action
from pyramid.response import Response

from controllers.base_controller import BaseController
from models.datasets import DatasetModel as dataset_model
from config import *

class DatasetsController(BaseController):
    def __init__(self, request):
        self.request = request
        self.response = request.response

    @action(renderer="./templates/tests.mako")
    def tests(self):
        return {'web_info': web_info}

    @action(renderer="./templates/datasets/search.mako")
    def search(self):
        search_criteria = self.request.GET.get('filter', '')

        if search_criteria:
            breakdown_datasets = {}
            result = dataset_model.simple_search(search_criteria)
            datasets_detail = result['datasets_detail']
            categories_detail = result['categories_detail']
            samples_detail = result['samples_detail']
        else:
            breakdown_datasets = dataset_model.breakdown_of_all_datasets()
            datasets_detail = {}
            categories_detail = {}
            samples_detail={}

        return {
                    'datasets': datasets_detail,
                    'categories': categories_detail,
                    'samples': samples_detail,
                    'breakdown_datasets': breakdown_datasets,
                    'criteria': search_criteria,
                    'web_info': web_info
                }

    # search_result_panel.mako used here is the same file used in search.mako
    # Todo: Return json rahter than a page in the future
    @action(renderer="./templates/datasets/search_result_panel.mako")
    def filter_categories_selection(self):
        search_criteria = self.request.GET.get('filter', '')
        ds_ids = json.loads(self.request.GET.get('ds_ids'))
        result = dataset_model.simple_search(search_criteria, ds_ids);

        datasets_detail = result['datasets_detail']
        categories_detail = result['categories_detail']
        samples_detail = result['samples_detail']

        return {'datasets': datasets_detail, 'categories': categories_detail, 'samples': samples_detail, 'criteria': search_criteria, 'web_info': web_info}

    @action(renderer='./templates/datasets/summary.mako')
    def view(self):
        # extract ds_id
        ds_id = self.request.matchdict.get('ds_id', None)
        dataset = dataset_model.get_dataset_metadata(ds_id)

        return {'dataset': dataset, 'web_info': web_info}

#   return only data
    @action(renderer='string')
    def export(self):
        export_type = self.request.GET.get('type')
        search_criteria = self.request.GET.get('filter', '')
        ds_ids = self.request.GET.get('ds_ids')

        del self.response.cache_control
        del self.response.pragma
        data_portal_version = 'v0.1'

        # setup response headers here for downloading data
        if export_type == 'datasets' or export_type == 'samples':
            self.response.content_type= 'text/tab-separated-values'
            self.response.content_disposition = 'attachment;filename=export_metadata_' + export_type + '_' + data_portal_version + '.tsv'
        else:
            self.response.content_type = 'text/plain'
            self.response.content_disposition = 'attachment;filename=multi_'+export_type+'_'+ data_portal_version +'.sh'
          
        self.response.charset= "utf8"
        download_data = dataset_model.export_downloadable_data(export_type, search_criteria, ds_ids);

        return download_data
