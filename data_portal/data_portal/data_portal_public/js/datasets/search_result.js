$(function(){
    $('.result-table')
        .DataTable({
            "ordering": false,
            "iDisplayLength": 100,
            "oLanguage": {
                  "sSearch": "Filter:"
                }
        });

    var toggle_status = true;
    register_click_event_on_toggle_col(toggle_status);
    register_click_event_on_export_btn();

    $('.clear-filter-btn').on('click', function(){
         $('input[type=checkbox].subcategory-checkbox:checked').prop('checked', false);
    });

    $('.reset-filter-btn').on('click', function(){
        $('.submit-btn').click();
    });

    $('.filter-btn').on('click', function(){
        selected_checkbox = $('input.subcategory-checkbox:checked');
        selected_checkbox_ids = [];
        _.forEach(selected_checkbox, function(item){
        	selected_checkbox_ids.push(item.id);
        })

        // Read out all selected ds_ids under each categories
        var ds_ids_under_cell_types = get_selected_ds_ids('input.cell-types');
        var ds_ids_under_tissue_types = get_selected_ds_ids('input.tissue-types');
        var ds_ids_under_species = get_selected_ds_ids('input.species');
        var ds_ids;

        var ids_array = [ds_ids_under_species, ds_ids_under_cell_types, ds_ids_under_tissue_types]
        // Here is to remove ds_ids (undefined) under unselected categories
        ids_array = _.reject(ids_array, function(ids) {
            return !ids;
        });
        // Unique ds_ids attached to subcategories under same category
        _.forEach(ids_array, function(each) {
             each = _.uniq(each)
        })

        // Using AND among different categories
        // Here is to get common ds_ids among different categories which has been selected
        if (ids_array.length == 1) {
            ds_ids = ids_array[0]
        }
        if (ids_array.length == 2) {
            ds_ids = _.intersection(ids_array[0], ids_array[1])
        }
        if (ids_array.length == 3) {
            ds_ids = _.intersection(ids_array[0], ids_array[1], ids_array[2])
        }
        ds_ids = JSON.stringify(ds_ids)

        var filter = $('#datasets-filter').val();
        var data = {
            filter: filter,
            ds_ids: ds_ids
        }

        // # Todo: Return json rahter than html in the future
        $.ajax({
             url: 'filter_categories_selection',
             data: data,
             dataType: 'html',
             type: 'GET',
             success: function(response) {
                //  this response returns a table-panel which is same to the panel that we have used in searching datasets
                 $('.result-panel').replaceWith(response);

                //  show selected checkboxes
                 _.forEach(selected_checkbox_ids, function(selected_checkbox_id){
                     $('#' + selected_checkbox_id).prop('checked', true);
                 })
             }
         })
    });

    function register_click_event_on_export_btn() {
        // Add listener to click event on export button
        $('.export-btn').on('click', function() {
            // Get all selected dataset_ids
            var selected_ds_ids = get_selected_ds_ids('tr.dataset > td > ')
            if (!selected_ds_ids) {
                selected_ds_ids = []
            }

            var filter = $('#datasets-filter').val();

            // append all dataset_ids as a query parammeter
            url = $(this).data('href');
            url += '&ds_ids=' + encodeURIComponent(selected_ds_ids) + '&filter=' + encodeURIComponent(filter);

            window.location = url
        })
    }

    function register_click_event_on_toggle_col(toggle_status) {
        $('.toggle-select-all').on('click', function() {
            if(toggle_status) {
                $('tr.dataset > td > input[type=checkbox]').prop('checked', false);
            }else {
                $('tr.dataset > td > input[type=checkbox]').prop('checked', true);
            }
            toggle_status = !toggle_status;
        })
        return toggle_status
    }

    function get_selected_ds_ids(pre_selector) {
        var selector = pre_selector + "[type=checkbox]:checked";
        var checkboxes = $(selector);

        var ds_ids;
        if (checkboxes.length != 0) {
            ds_ids = [];
        }
        _.forEach(checkboxes, function(checkbox){
            ids_str = checkbox.value;
            ids_json = ids_str.replace(/'/gi, "\"");
            ids = JSON.parse(ids_json);
            ds_ids = ds_ids.concat(ids);
        });

        return ds_ids;
    }
});
