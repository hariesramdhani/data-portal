<%inherit file="base.mako"/>

<%block name="partial_content">
    <%
        tests = [
            ['Home Page', 'Show all breakdown data types, founders and partners', '/datasets/search'],
            ['Search Results', 'Show all searched datasets & related categories', '/datasets/search?filter=bone+marrow'],
            ['Summary Page', 'Display summaries of the dataset and downloadable files', '/datasets/view/7021'],
            ['Yugene Download link', 'Show yugene only when show_yugene is not fasle', '/datasets/search?filter=filter=D%236150+D%236131+D%236798+D%236130']
        ]
    %>
    <link href=${web_info['web_path'] + "/css/tests.css"} rel="stylesheet">
    <script type="text/javascript" src=${web_info['web_path']+"/js/tests.js"}></script>
    ## <script src=${web_info['web_path'] + "/js/datasets/search_result.js"}></script>

    <div class="tests">
        <table id="tests-table">
            <thead class="container">
                <tr class="row">
                    <th class="md-col-3">Type</th>
                    <th class="md-col-8">Test</th>
                    <th class="md-col-1">Link</th>
                </tr>
            </thead>
            <tbody class="container">
            %for test in tests:
                <tr class="row">
                    <td class="md-col-3">${test[0]}</td>
                    <td class="md-col-8">${test[1]}</td>
                    <td class="md-col-1"><a class="link" href="${test[2]}">Link</a></td>
                </tr>
            %endfor
            </tbody>
        </table>
    </div>

</%block>
