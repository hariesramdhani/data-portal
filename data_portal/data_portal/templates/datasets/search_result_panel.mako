<link href=${web_info['web_path'] + "/css/datasets/search_result_panel.css"} rel="stylesheet">
<script src=${web_info['web_path'] + "/js/datasets/search_result.js"}></script>
<div class="result-panel">
    <div class="category-panel">
        % if len(datasets) != 0:
            <h4>All Categories</h4>
            <% list_of_valid_categories = ['Species', 'Cell Types', 'Tissue Types'] %>
            %for category_name in list_of_valid_categories:
                <% class_name = category_name.lower().replace(' ', '-') %>
                <div class='category ${class_name}'>
                    %if categories[category_name]:
                        <span class='category-name'>${category_name}</span>
                        <ul style="list-style-type:none">
                            <% subcategories = categories[category_name] %>
                            %for subcategory_name, ds_ids in subcategories.items():
                            <!-- Hidden cell types by using css at the moment. The names of cell types are too long, so we will need to handle its css later -->
                                <li class="subcategory" data-ds-ids="${ds_ids}">
                                    <input id="${subcategory_name.lower().replace(' ', '-')}" type="checkbox" class="subcategory-checkbox ${class_name}" name ="${category_name}" value="${ds_ids}" />
                                    ${subcategory_name} (${len(ds_ids)})
                                </li>
                            %endfor
                        </ul>
                    %endif
                </div>
            %endfor
            <div class="button-group">
                <button class="btn btn-default filter-btn">Filter</button>
                <button class="btn btn-default clear-filter-btn">Clear</button>
                <button class="btn btn-default reset-filter-btn">Reset</button>
            </div>
        %else:
            &nbsp;
        %endif
    </div>
    <%include file="./datasets_table.mako"/>
</div>
