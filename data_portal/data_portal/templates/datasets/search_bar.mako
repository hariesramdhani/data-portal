<link href=${web_info['web_path'] + "/css/datasets/search_bar.css"} rel="stylesheet">
<div class="full-text-search-bar">
    <form class="search-form" action="/datasets/search" method="get">
        <div class="full-text-search">
            <label class="search-term">Enter search terms:</label>
            %if criteria:
                <input placeholder='Try to search: "bone marrow" monocyte D#1000' id="datasets-filter" type="text" name="filter" value="${criteria}"><br>
    	    %else:
    	        <input placeholder='Try to search: "bone marrow" monocyte D#1000' id="datasets-filter" type="text" name="filter" value=""><br>
    	    %endif
        </div>
        <button type="submit" class="btn btn-default submit-btn">Search</button>
    </form>
    % if not criteria:
        <div class="breakdown-datasets">
            <span class="title">Data Type Breakdown</span>
            <table class="breakdown-table">
                <thead>
                    <tr>
                        <th class="col-lg-6">Data Type</th>
                        <th class="col-lg-4">Count</th>
                    </tr>
                </thead>
                <tbody>
                    %for breakdown_dataset in breakdown_datasets:
                        <tr>
                            <td>${breakdown_dataset['data_type']}</td>
                            <td>${breakdown_dataset['count']}</td>
                        </tr>
                    %endfor
                </tbody>
            </table>
        </div>
        <div class="founders-panel">
            <span class="title">Funders and Partners</span>
            <div class="founder-list">
                <img id="linux-australia-logo" src="/images/linux-australia-logo.png">
                <img id="lstem-cells-australia-logo" src="/images/stem-cells-australia-logo.jpg">
                <img id="nectarcloud-logo" src="/images/nectarcloud.png">
                <img id="university-of_melbourne-logo" src="/images/University_of_Melbourne_logo.png">
            </div>
        </div>
    % endif
</div>
