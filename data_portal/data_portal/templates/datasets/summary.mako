<%inherit file="../base.mako"/>

    ## Only show summary box and file downloads box here
<%block name="partial_content">
    <%!
        def setup_accession_ids_for_viewing(dataset_metadata):
            ae_id = dataset_metadata['ae_accession_id']
            geo_id = dataset_metadata['geo_accession_id']
            sra_id = dataset_metadata['sra_accession_id']
            pxd_id = dataset_metadata['pxd_accession_id']
            ena_id = dataset_metadata['ena_accession_id']
            accession_urls = {}

            if ae_id != 'N/A' and ae_id != 'NULL' and ae_id != '' :
                accession_urls[ae_id] = "http://www.ebi.ac.uk/arrayexpress/experiments/" + ae_id
            if geo_id != 'N/A' and geo_id != 'NULL' and geo_id != '' :
                accession_urls[geo_id] = "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=" + geo_id

            if sra_id != 'N/A' and sra_id != 'NULL' and sra_id != '' :
                accession_urls[sra_id] = "https://trace.ncbi.nlm.nih.gov/Traces/sra/?study=" + sra_id

            if pxd_id != 'N/A' and pxd_id != 'NULL' and pxd_id != '' :
                accession_urls[pxd_id] = "http://proteomecentral.proteomexchange.org/cgi/GetDataset?ID=" + pxd_id

            if ena_id != 'N/A' and ena_id != 'NULL' and ena_id != '' :
                accession_urls[ena_id] = "http://www.ebi.ac.uk/ena/data/view/" + ena_id

            return accession_urls
    %>

    <link href=${web_info['web_path'] + "/css/datasets/summary.css"} rel="stylesheet">
    <div class="row">
        <div class="dataset-summary-box col-md-7">
            <% ds_id = list(dataset.keys())[0] %>
            <% ds = dataset[ds_id] %>
            <div class="title">${ds['title']}</div>
            <div class="handle text"> ${ds['handle']} (${ds['organism']})</div>
            <div class="cells text">${ds['cells_samples_assayed']}</div>
            <div class="description text">${ds['authors']}</div>
            <div class="description text">${ds['description']}</div>
            <div class="dataset-links-col">
                <table>
                    <tbody class="row">
                        <tr class="pubMedID text">
                            <td class="col-md-4">Pubmed ID:</td>
                            <td class="detail col-md-8">
                                <a target="_blank" href="${'http://www.ncbi.nlm.nih.gov/pubmed/' + ds['pub_med_id']}">${ds['pub_med_id']}</a>
                            </td>
                        </tr>
                        <tr class="accessionID text">
                            <td class="col-md-4">Accession IDs:</td>
                            <td class="detail col-md-8">
                                <% from mako.template import Template %>
                                <% url_list = setup_accession_ids_for_viewing(ds)%>
                                <% urls_count = len(url_list.keys())%>
                                <% index = 1%>
                                %for accession_id, url in url_list.items():
                                    <a target="_blank" href="${url}">${accession_id}</a>
                                    %if index < urls_count:
                                        <span> | </span>
                                    %endif
                                    <% index += 1%>
                                %endfor
                            </td>
                        </tr>
                        <tr class="name text">
                            <td class="col-md-4">Name:</td>
                            <td class="detail col-md-8">${ds['name']}</td
                        </tr>
                        <tr class="email text">
                            <td class="col-md-4">Email:</td>
                            <td class="detail col-md-8">${ds['email']}</td
                        </tr>
                        <tr class="affiliation text">
                            <td class="col-md-4">Affiliation:</td>
                            <td class="detail col-md-8">${ds['affiliation']}</td
                        </tr>
                        <tr class="sample-numbers text">
                            <td class="col-md-4">Number of Samples:</td>
                            <td class="detail col-md-8">${ds['number_of_samples']}</td>
                        </tr>
                        <tr class="platform text">
                            <td class="col-md-4">Platform:</td>
                            <td class="detail col-md-8">${ds['platform']}</td
                        </tr>
                    </tbody>
                </table>
            </div>
            <button class="link-back-btn" data-href=${web_info['web_path'] + "/datasets/export?type=datasets"}>
                <a  target="_blank" href="${web_info['stemformatics_web_path']}/datasets/view/${ds_id}">Link back to Stemformatics</a>
            </button>
        </div>

        ## Inside this box, list out all download links of downloadable files: .gct, .Rohart, .probes, .bgonly_cumulative.txt
        <div class="file-downloads-box col-md-4">
            <div class="title">
                Reports
            </div>
            <div class="breakdown">
                <table id="downloadable-files-table row">
                    <thead>
                        <tr>
                          <th class="download-th col-lg-2">
                            All downloadable files
                          </th>
                          <th class="download-th col-lg-1">
                            Link to Files
                          </th>
                        </tr>
                    </thead>
                    <tbody>
                        <% files = ['dp_gct_file_location', 'dp_yugene_file_location', 'dp_probes_file_location', 'dp_rohart_file_location'] %>
                        <% downloadeable_file_count = 0%>
                        %for file in files:
                            ## Hide all rohart files if not human
                            <% human_rohart_file = (file == 'dp_rohart_file_location' and ds['organism'] == 'Homo sapiens') %>
                            <% keys_exist_in_ds = (file in ds) and ('dp_base_url' in ds) and ('dp_ensembl_version' in ds) %>
                            %if keys_exist_in_ds and ds[file] and ((file != 'dp_rohart_file_location') or (huaman_rohart_file)):
                                ## Don't show yugene file if show_yugene is not True
                                %if file == 'dp_yugene_file_location' and (not ds['show_yugene']):
                                    <% continue %>
                                %endif
                                <% downloadeable_file_count += 1%>
                                <tr>
                                    <td class="download-td col-lg-2"> ${ds[file]} </td>
                                    <td class="download-td col-lg-1">
                                        <a target="_blank" href="${ds['dp_base_url'] + ds['dp_ensembl_version'] + '/' +ds[file]}">Download</a>
                                    </td>
                                </tr>
                            %endif
                        %endfor
                        % if downloadeable_file_count == 0:
                            <tr>
                                <td class="no-downloadable-files-td" colspan="2">No downloadable files available.</td>
                            </tr>
                        %endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</%block>
