import sys


sys.path.insert(0, '../data_portal')
from models.datasets import DatasetModel

def test_search_given_empty_input():
    results = DatasetModel.simple_search('  ')
    datasets_detail = results['datasets_detail']
    ids = datasets_detail.keys()

    assert len(ids)  == 0

def test_search_given_single_word():
    results = DatasetModel.simple_search('7021')
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) >= 3

    assert '6001' in ids
    assert datasets_detail['6001']['handle'] == 'Fasano_2010_20362538'
    assert datasets_detail['6001']['data_type'] == 'Microarray'
    assert datasets_detail['6001']['title'] == 'Efficient derivation of functional floor plate tissue from human embryonic stem cells'
    assert datasets_detail['6001']['species'] == 'Homo sapiens'
    assert len(datasets_detail['6001']['matched_samples']) == 1
    assert datasets_detail['6001']['total_samples_count'] == 30

    assert '6776' in ids
    assert datasets_detail['6776']['handle'] == 'Iwayama_2015_26019175'
    assert datasets_detail['6776']['data_type'] == 'RNASeq'
    assert datasets_detail['6776']['title'] == 'PDGFRα signaling drives adipose tissue fibrosis by targeting progenitor cell plasticity'
    assert datasets_detail['6776']['species'] == 'Mus musculus'
    assert len(datasets_detail['6776']['matched_samples']) == 0
    assert datasets_detail['6776']['total_samples_count'] == 6

    assert '7012' in ids
    assert datasets_detail['7012']['handle'] == 'Sarkar_2016_27498859_a'
    assert datasets_detail['6776']['data_type'] == 'RNASeq'
    assert datasets_detail['7012']['title'] == 'Sox2 suppresses gastric tumorigenesis in mice'
    assert datasets_detail['7012']['species'] == 'Mus musculus'
    assert len(datasets_detail['7012']['matched_samples']) == 0
    assert datasets_detail['7012']['total_samples_count'] == 6

def test_search_by_counting_matched_sample_given_single_word():
    results = DatasetModel.simple_search('msc')
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()
    matched_samples = datasets_detail['6272']['matched_samples']

    assert len(ids) >= 115
    assert len(matched_samples) == 9

def test_search_given_multiple_words():
    results = DatasetModel.simple_search('7021 7115')
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) >= 4
    assert '6162' in ids
    assert '7012' in ids
    assert '6776' in ids
    assert '6001' in ids

def test_search_given_symbols():
    results = DatasetModel.simple_search('????')
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids)  == 0

# Pass in an object (see if it breaks)
def test_search_given_an_object():
    object_input = "'1000': {'handle':'**', 'title': 'No title', 'total_samples_count': 100}, 'species': 'Homo sapiens'}"
    results = DatasetModel.simple_search(object_input)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) == 0

def test_search_given_long_string_input():
    long_input_string = '3047124561569845623495632945634'
    results = DatasetModel.simple_search(long_input_string)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) == 0

# Test support for searching by ds_id (e.g. D#7021)
def test_search_given_a_ds_id():
    ds_id = 'D#7021'
    results = DatasetModel.simple_search(ds_id)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert sorted(ids) == ['7021']
    assert datasets_detail['7021']['handle'] == 'Agerstam_2010_20554971'
    assert datasets_detail['7021']['title'] == 'Modeling the human 8p11-myeloproliferative syndrome in immunodeficient mice.'
    assert datasets_detail['7021']['species'] == 'Homo sapiens'
    assert len(datasets_detail['7021']['matched_samples']) == 12
    assert datasets_detail['7021']['total_samples_count'] == 12

def test_search_given_multiple_ds_ids():
    ds_id = 'D#7021 D#6147 D#6153'
    results = DatasetModel.simple_search(ds_id)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert sorted(ids) == sorted(['6147', '6153', '7021'])

def test_search_given_empty_ds_id():
    ds_id = 'D#'
    results = DatasetModel.simple_search(ds_id)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) == 0

def test_search_given_wrong__ds_id():
    ds_id = 'D#12d3'
    results = DatasetModel.simple_search(ds_id)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) == 0

def test_search_given_wrong_ds_id():
    ds_id = 'D#123456789'
    results = DatasetModel.simple_search(ds_id)
    datasets_detail = results['datasets_detail']

    ids = datasets_detail.keys()

    assert len(ids) == 0

def test_advanced_given_ds_id_and_word():
    mixing_search_criteria = 'D#7021 mincle'
    direct_results = DatasetModel.simple_search(mixing_search_criteria)
    direct_datasets_detail = direct_results['datasets_detail']
    direct_ids = direct_datasets_detail.keys()

    result_searched_by_ds_ids = DatasetModel.simple_search('D#7021')
    datasets_detail = result_searched_by_ds_ids['datasets_detail']
    ids = list(datasets_detail.keys())

    result_searched_by_words = DatasetModel.simple_search('mincle')

    ids.extend(list(result_searched_by_words['datasets_detail'].keys()))
    ids = list(set(ids))

    assert sorted(ids) == sorted(direct_ids)

def test_search_given_single_double_quoted_string():
    double_quoted_string = ' \"bone marrow\" '
    results = DatasetModel.simple_search(double_quoted_string)

    datasets_detail = results['datasets_detail']
    ids = datasets_detail.keys()

    assert len(ids) >= 80
    assert '6658' in ids

def test_search_given_multiple_double_quoted_string():
    double_quoted_string = ' \"bone marrow\" \"homo\"'
    results = DatasetModel.simple_search(double_quoted_string)

    datasets_detail = results['datasets_detail']
    ids = datasets_detail.keys()

    assert len(ids) >= 262
    assert '6658' in ids

def test_search_given_double_quoted_words_and_dataset_id_and_words():
    double_quoted_string = ' \"bone marrow\"  D#2000  20644536'
    results = DatasetModel.simple_search(double_quoted_string)

    datasets_detail = results['datasets_detail']
    ids = datasets_detail.keys()

    assert len(ids) >= 82
    assert '6658' in ids
    assert '2000' in ids
    assert '6146' in ids
    assert '6147' in ids

#Check if all counts of subcategories under Species are correct
def test_subcategories_count_when_search_one_dataset():
    ds_id = 'D#7021'
    results = DatasetModel.simple_search(ds_id)
    categories_detail = results['categories_detail']

    species = categories_detail['Species']
    species_count = len(species.keys())

    tissue_types = categories_detail['Tissue Types']
    tissue_types_count = len(tissue_types.keys())

    tissue_types = categories_detail['Tissue Types']
    tissue_types_count = len(tissue_types.keys())

    cell_types = categories_detail['Cell Types']
    cell_types_count = len(cell_types.keys())

    assert species_count == 1
    assert species['Homo sapiens'] == ['7021']
    assert tissue_types_count == 1
    assert tissue_types['umbilical cord blood'] == ['7021']
    assert cell_types_count == 4
    assert cell_types['ZMYM2-FGFR1 transduced haematopoietic stem and progenitor cells'] == ['7021']
    assert cell_types['BCR-ABL1 transduced haematopoietic stem and progenitor cells'] == ['7021']
    assert cell_types['BCR-FGFR1 transduced haematopoietic stem and progenitor cells'] == ['7021']
    assert cell_types['haematopoietic stem and progenitor cells'] == ['7021']

#Check if all counts of subcategories under Cell Types are correct
def test_subcategories_count_of_cell_types():
    ds_id = 'D#7021'
    results = DatasetModel.simple_search(ds_id)
    categories_detail = results['categories_detail']
    cell_types = categories_detail['Cell Types']

    assert cell_types['BCR-ABL1 transduced haematopoietic stem and progenitor cells'] ==  ['7021']
    assert cell_types['BCR-FGFR1 transduced haematopoietic stem and progenitor cells'] ==  ['7021']
    assert cell_types['haematopoietic stem and progenitor cells'] == ['7021']
    assert cell_types['ZMYM2-FGFR1 transduced haematopoietic stem and progenitor cells'] ==  ['7021']

#Check if all counts of subcategories under Tissue Types are correct
def test_subcategories_count_of_tissue_types():
    ds_id = 'D#7021'
    results = DatasetModel.simple_search(ds_id)
    categories_detail = results['categories_detail']

    tissue_types = categories_detail['Tissue Types']

    assert tissue_types['umbilical cord blood'] == ['7021']

#Check if counts are zero, then no results are returned
def test_subcategories_count_when_no_results_are_return():
    ds_id = ' '
    results = DatasetModel.simple_search(ds_id)
    categories_detail = results['categories_detail']

    species_count_array = categories_detail['Species'].values()
    tissue_types_count_array = categories_detail['Tissue Types'].values()
    cell_types_count_array = categories_detail['Cell Types'].values()

    assert len(species_count_array) == 0
    assert len(tissue_types_count_array) == 0
    assert len(cell_types_count_array) == 0

def test_matched_category_details_after_search():
    search_criteria = 'D#7021 D#6147 D#6153'
    result = DatasetModel.simple_search(search_criteria)

    datasets_detail = result['datasets_detail']
    categories_detail = result['categories_detail']

    assert '6153' in categories_detail['Species']['Homo sapiens']
    assert '7021' in categories_detail['Species']['Homo sapiens']
    assert '6147' in categories_detail['Species']['Mus musculus']

    assert '7021' in  categories_detail['Tissue Types']['umbilical cord blood']
    assert '6153' in  categories_detail['Tissue Types']['in vitro']
    assert '6153' in  categories_detail['Tissue Types']['fetal lung']
    assert '6147' in  categories_detail['Tissue Types']['NULL']

    assert '6153' in categories_detail['Cell Types']['Fibroblast']
    assert '6153' in categories_detail['Cell Types']['embryonic stem cell']
    assert '6147' in categories_detail['Cell Types']['miPSC']
    assert '7021' in categories_detail['Cell Types']['BCR-FGFR1 transduced haematopoietic stem and progenitor cells']
    assert '7021' in categories_detail['Cell Types']['BCR-ABL1 transduced haematopoietic stem and progenitor cells']
    assert '7021' in categories_detail['Cell Types']['haematopoietic stem and progenitor cells']
    assert '7021' in categories_detail['Cell Types']['ZMYM2-FGFR1 transduced haematopoietic stem and progenitor cells']

def test_categories_detail_after_filtering():
    search_criteria = 'bone marrow'
    ds_ids = ['6786']
    result = DatasetModel.simple_search(search_criteria, ds_ids)

    datasets_detail = result['datasets_detail']
    categories_detail = result['categories_detail']

    ids = datasets_detail.keys()
    species = categories_detail['Species'].keys()
    tissue_types = categories_detail['Tissue Types'].keys()

    assert len(ids) == 1
    assert '6786' in ids
    assert len(datasets_detail['6786']['matched_samples']) == 31

    assert len(species) == 1
    assert 'Homo sapiens' in species

    assert len(tissue_types) == 3
    assert 'Bone marrow' in tissue_types
    assert 'Skeletal Muscle' in tissue_types
    assert 'in vitro' in tissue_types

#check if get all needed dataset metadata in the summary package with a valid ds_id
def test_dataset_metadata_when_given_valid_ds_id():
    ds_id = '3000';
    returnDict = DatasetModel.get_dataset_metadata(ds_id)

    title = 'Aberrant luminal progenitors as the candidate target population for basal tumor development in BRCA1 mutation carriers.'
    handle = 'Lim_2009_19648928'
    organism = 'Homo sapiens'
    cells_samples_assayed = 'stromal cells,mammary stem cells (MaSC),luminal progenitor cells,mature luminal cells'
    authors = 'Lim E, Vaillant F, Wu D, Forrest NC, Pal B, Hart AH, Asselin-Labat ML, Gyorki DE, Ward T, Partanen A, Feleppa F, Huschtscha LI, Thorne HJ, kConFab, Fox SB, Yan M, French JD, Brown MA, Smyth GK, Visvader JE, Lindeman GJ'
    description = "Basal-like breast cancers arising in women carrying mutations in the BRCA1 gene, encoding the tumor suppressor protein BRCA1, are thought to develop from the mammary stem cell. " \
                "To explore early cellular changes that occur in BRCA1 mutation carriers, we have prospectively isolated distinct epithelial subpopulations from normal mammary tissue and preneoplastic specimens " \
                "from individuals heterozygous for a BRCA1 mutation. We describe three epithelial subsets including basal stem/progenitor, luminal progenitor and mature luminal cells. Unexpectedly, we found that " \
                "breast tissue from BRCA1 mutation carriers harbors an expanded luminal progenitor population that shows factor-independent growth in vitro. Moreover, gene expression profiling revealed that breast " \
                "tissue heterozygous for a BRCA1 mutation and basal breast tumors were more similar to normal luminal progenitor cells than any other subset, including the stem cell-enriched population. The c-KIT " \
                "tyrosine kinase receptor (encoded by KIT) emerged as a key marker of luminal progenitor cells and was more highly expressed in BRCA1-associated preneoplastic tissue and tumors. Our findings suggest " \
                "that an aberrant luminal progenitor population is a target for transformation in BRCA1-associated basal tumors."
    pub_med_id = '19648928'
    name = 'Geoffrey J. Lindeman, Jane E. Visvader'
    email = 'lindeman@wehi.edu.au, visvader@wehi.edu.au'
    affiliation = 'Walter and Eliza Hall Institute of Medical Research'
    number_of_samples = 12

    assert returnDict[ds_id]['title'] == title
    assert returnDict[ds_id]['handle'] == handle
    assert returnDict[ds_id]['organism'] == organism
    assert returnDict[ds_id]['cells_samples_assayed'] == cells_samples_assayed
    assert returnDict[ds_id]['authors'] == authors
    assert returnDict[ds_id]['pub_med_id'] == pub_med_id
    assert returnDict[ds_id]['name'] == name
    assert returnDict[ds_id]['email'] == email
    assert returnDict[ds_id]['affiliation'] == affiliation
    assert returnDict[ds_id]['number_of_samples'] == number_of_samples

# Check the string only including one hear line, when export selected dataset metadata without selecting datasets
def test_only_return_header_line_when_export_selected_dataset_metadata_without_selecting_datasets():
    export_type = 'datasets'
    search_criteria = 'bone marrow'
    ds_ids = ''

    data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)

    row_list = ['ds_id','handle','title','organism','samples found','all_samples']
    expected_data = "\t".join(row_list) + "\n"

    assert data == expected_data

# Check the returned data, when export selected dataset metadata with selecting one datasets
def test_return_data_when_export_selected_dataset_metadata_with_selecting_one_dataset():
    export_type = 'datasets'
    search_criteria = 'bone marrow'
    ds_ids = '6329'

    data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)

    row_list = ['ds_id','handle','title','organism','samples found','all_samples']
    expected_data = "\t".join(row_list) + "\n"
    row_list= [
                 '6329',
                 'McWeeney_2010_19837975',
                 'A gene expression signature of CD34+ cells to predict major cytogenetic response in chronic-phase chronic myeloid leukemia patients treated with imatinib',
                 'Homo sapiens',
                 '36',
                 '59'
             ]
    expected_data += "\t".join(row_list) + "\n"

    assert data == expected_data

# Check the returned data, when export selected dataset metadata with selecting multiple datasets
def test_return_data_when_export_selected_dataset_metadata_with_selecting_multiple_dataset():
        export_type = 'datasets'
        search_criteria = 'bone marrow'
        ds_ids = '6329, 6310'

        data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)

        row_list = ['ds_id','handle','title','organism','samples found','all_samples']
        expected_data = "\t".join(row_list) + "\n"
        row_list= [
                     '6329',
                     'McWeeney_2010_19837975',
                     'A gene expression signature of CD34+ cells to predict major cytogenetic response in chronic-phase chronic myeloid leukemia patients treated with imatinib',
                     'Homo sapiens',
                     '36',
                     '59'
                 ]
        expected_data += "\t".join(row_list) + "\n"
        row_list= [
                    '6310',
                    'Mackay_2013_24162776',
                    'Development pathway for skin resident memory CD103+CD8+ T cells',
                    'Mus musculus',
                    '0',
                    '24'
                ]
        expected_data += "\t".join(row_list) + "\n"

        assert data == expected_data

# Check the string only including one hear line, when export selected sample metadata without selecting datasets
def test_only_return_header_line_when_export_selected_sample_metadata_without_selecting_datasets():
    export_type = 'samples'
    search_criteria = 'bone marrow'
    ds_ids = ''

    actual_data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)

    row_list = ['species','ds_id','handle','chip_id','sample_id','sample_type']
    expected_data = "\t".join(row_list) + "\n"

    assert actual_data == expected_data

# Check the string only including one hear line, when export selected sample metadata without selecting datasets
def test_only_return_header_line_when_selected_dataset_has_no_sample_metadata():
    export_type = 'samples'
    search_criteria = 'bone marrow'
    ds_ids = '6541'

    actual_data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)

    row_list = ['species','ds_id','handle','chip_id','sample_id','sample_type']
    expected_data = "\t".join(row_list) + "\n"

    assert actual_data == expected_data

# Check the returned data, when export selected dataset metadata with selecting one datasets
def test_return_data_when_export_selected_sample_metadata_with_selecting_one_dataset():
    export_type = 'samples'
    search_criteria = 'bone marrow'
    ds_ids = '6146'

    actual_data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)

    row_list = ['species','ds_id','handle','chip_id','sample_id','sample_type']
    expected_data = "\t".join(row_list) + "\n"
    row_list= [
                 'Mus musculus',
                 '6146',
                 'Polo_2010_20644536_a',
                 'GSM565753',
                 'Granulocyte-derived iPS, replicate 1',
                 'Granulocyte-derived iPS'
             ]
    expected_data += "\t".join(row_list) + "\n"
    row_list= [
                 'Mus musculus',
                 '6146',
                 'Polo_2010_20644536_a',
                 'GSM565754',
                 'Granulocyte-derived iPS, replicate 2',
                 'Granulocyte-derived iPS'
             ]
    expected_data += "\t".join(row_list) + "\n"
    row_list= [
                 'Mus musculus',
                 '6146',
                 'Polo_2010_20644536_a',
                 'GSM565755',
                 'Granulocyte-derived iPS, replicate 3',
                 'Granulocyte-derived iPS'
             ]
    expected_data += "\t".join(row_list) + "\n"

    assert actual_data == expected_data

def test_get_all_metadata_for_dataset_by_id():
    input_id = 1000
    results =DatasetModel.get_all_metadata_for_dataset_by_id(input_id)

    assert len(results.keys())== 1


def test_get_all_metadata_for_dataset_by_value():
    input_id = 2000
    results =DatasetModel.get_all_metadata_for_dataset_by_id(input_id)

    assert len(results[input_id]) == 3

def test_get_all_metadata_for_dataset_by_value_datasets():
    input_id = 2000
    results =DatasetModel.get_all_metadata_for_dataset_by_id(input_id)

    ids=results[input_id].keys()
    assert 'datasets' in ids

def test_get_all_metadata_for_dataset_by_value_dataset_md():
    input_id = 2000
    results =DatasetModel.get_all_metadata_for_dataset_by_id(input_id)

    ids=results[input_id].keys()
    assert 'dataset_metadata' in ids

def test_get_all_metadata_for_dataset_by_value_biosamples_md():
    input_id = 20
    results =DatasetModel.get_all_metadata_for_dataset_by_id(input_id)

    ids=results[input_id].keys()
    assert 'biosamples_metadata' in ids


def test_breakdown_of_all_datasets():
    results =DatasetModel.breakdown_of_all_datasets()

    assert len(results) == 6

def test_create_download_script_for_multiple_datasets():
    export_type = 'download_script'
    search_criteria = 'mincle'
    ds_ids = '6731,6498'

    actual_data = DatasetModel.export_downloadable_data(export_type, search_criteria, ds_ids)


    expected_data = "#!/bin/bash\n"
    expected_data += "####################################################################\n"
    expected_data += "# This is a script to download from a Mac/Linux/Unix computer\n"
    expected_data += "# You will need to have wget installed \n"
    expected_data += "# The following commands assume that you are on a terminal in the same directory as the script\n"
    expected_data += "# You will need to make this script executable by using the command below\n"
    expected_data += "# chmod 744 multi_download_script.sh \n"
    expected_data += "# You will need to run by using the command below \n"
    expected_data += "# ./multi_download_script.sh \n"
    expected_data += "####################################################################\n"
    expected_data += 'wget "https://swift.rc.nectar.org.au:8888/v1/AUTH_cf717aa273b94ec3bd55c6527a53278a/data_portal_6498/ensembl_v69/dataset6498.cumulative.txt\" -O 6498_Tanaka_2014_25236782.yugene.txt\n'
    expected_data += 'wget "https://swift.rc.nectar.org.au:8888/v1/AUTH_cf717aa273b94ec3bd55c6527a53278a/data_portal_6498/ensembl_v69/dataset6498.gct\" -O 6498_Tanaka_2014_25236782.gct\n'
    expected_data += 'wget "https://swift.rc.nectar.org.au:8888/v1/AUTH_cf717aa273b94ec3bd55c6527a53278a/data_portal_6731/ensembl_v69/dataset6731.cumulative.txt\" -O 6731_Arumugam_2015_S4M-6731.yugene.txt\n'
    expected_data += 'wget "https://swift.rc.nectar.org.au:8888/v1/AUTH_cf717aa273b94ec3bd55c6527a53278a/data_portal_6731/ensembl_v69/dataset6731.gct\" -O 6731_Arumugam_2015_S4M-6731.gct\n'
 
    assert actual_data==expected_data
