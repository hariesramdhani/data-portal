# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is created for Stemformatics Data Portal
Version 1.0 (Prototype 1), it includes Pyramid web app with basic search on datasets 
and have cornice API interface to get, set the datasets in PostgreSQL DB. 


### How do I get set up? ###

* Dependencies: 
  Check list_of_requirements.txt file in the repo
  Run: pip install -r list_of_requirements.txt in the virtual env

* How to run tests
  Run: nosetests ../nosetests/test_datasets.py

* Deployment instructions

### Contribution guidelines ###

* Writing tests
Go to the nosetests folder and write unit tests of your datasets model/controller.
* Code review
Rowland Mosbergen and Isha Nagpal



For more information contact Huan Wang, Sadia Waleem or Rowland Mosbergen 
